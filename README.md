# Calling a Rust binary from Drupal example

## Dependencies

Install musl, on Debian based systems:

```bash
apt install musl musl-tools
```

Install `rust` using instructions at https://rustup.rs

## Instructions to replicate the demo

1. In Pantheon, create a new site. I called mine `rusttest`, and used Pantheon's Drupal 10 template.
2. `git clone` the site and `cd` into it.
3. `composer update`
4. `mkdir bin`
5. `cargo new hello_world --bin`
6. `cd hello_world`
7. `rustup target add x86_64-unknown-linux-musl`
8. `cargo build --target=x86_64-unknown-linux-musl`
9. `mv target/x86_64-unknown-linux-musl/debug/hello_world ../bin/`
10. `echo 'target' > .gitignore`
11. Create a custom module that calls out to Rust, using: `web/modules/custom/hello` either as a reference or just copy/paste.
12. `cd .. && git add .`
13. `git commit -m "Git 'er done! *sensible chuckle*."`
14. `git push`
15. Go to your Pantheon site, install it if you haven't already done so, then enable the 'Rust Hello World' module.

