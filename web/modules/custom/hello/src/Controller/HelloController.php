<?php

namespace Drupal\hello\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Markup;

class HelloController extends ControllerBase {

  public function runCommand() {
    $output = shell_exec('/code/bin/hello_world 2>&1');
    $markupOutput = Markup::create(nl2br($output));
    return [
      '#markup' => $this->t('Command output: @output', ['@output' => $markupOutput]),
    ];
  }

}

